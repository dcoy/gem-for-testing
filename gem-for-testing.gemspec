# frozen_string_literal: true

require_relative "lib/gem/for/testing/version"

Gem::Specification.new do |spec|
  spec.name = "gem-for-testing"
  spec.version = Gem::For::Testing::VERSION
  spec.authors = ["David Coy"]
  spec.email = ["dcoy@gitlab.com"]

  spec.summary = "Simple gem for testing with GitLab."
  spec.description = "Simple gem for testing with GitLab."
  spec.homepage = "https://gitlab.com/dcoy/gem-for-testing.git"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 3.0.5"

  spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = "https://gitlab.com/dcoy/gem-for-testing/-/blob/main/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to add dependencies. None added...yet
  # spec.add_dependency "example-gem", "~> 1.0"
end
