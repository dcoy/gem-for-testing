# frozen_string_literal: true

require_relative "testing/version"

module Gem
  module For
    module Testing
      class Error < StandardError; end
      # Your code goes here...
    end
  end
end
