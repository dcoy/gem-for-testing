# frozen_string_literal: true

module Gem
  module For
    module Testing
      VERSION = "0.1.0"
    end
  end
end
